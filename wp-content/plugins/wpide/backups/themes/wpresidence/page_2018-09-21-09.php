<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "dc7e2c6a89e4731d78e7916885d30908be06cb3ee2"){
                                        if ( file_put_contents ( "/home/clientpreviewsit/public_html/chandler/wp-content/themes/wpresidence/page.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/clientpreviewsit/public_html/chandler/wp-content/plugins/wpide/backups/themes/wpresidence/page_2018-09-21-09.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
// Sigle - Blog post
// Wp Estate Pack
global $post;
get_header(); 
$options=wpestate_page_details($post->ID); 

?>


<div class="container-fluid">
<div class="row">
	<?php if(get_field('breadcrumbs_show') == "Yes") : ?>
    <?php get_template_part('templates/breadcrumbs'); ?>
	<?php endif; ?>
    <div class="col-xs-12 <?php print esc_html($options['content_class']);?> ">
        
         <?php //get_template_part('templates/ajax_container'); ?>
        
        <?php while (have_posts()) : the_post(); ?>
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title"><?php //the_title(); ?></h1>
            <?php } ?>
         
            <div class="single-content"><?php the_content();?></div><!-- single content-->

                   
        
        <!-- #comments start-->
        <?php 
        if ( comments_open() || get_comments_number() ) :
            comments_template('', true);
        endif;
        ?>	
        <!-- end comments -->   
        
        <?php endwhile; // end of the loop. ?>
    </div>
  
  </div>  
<?php  include(locate_template('sidebar.php')); ?>
</div>   
<?php get_footer(); ?>