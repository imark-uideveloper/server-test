<?php
// Sigle - Blog post
// Wp Estate Pack
global $post;
get_header(); 
$options=wpestate_page_details($post->ID); 

?>


<div class="container-fluid">
<div class="row">
	<?php if(get_field('breadcrumbs_show') == "Yes") : ?>
    <?php get_template_part('templates/breadcrumbs'); ?>
	<?php endif; ?>
    <div class="col-xs-12 <?php print esc_html($options['content_class']);?> ">
        <?php if(get_field('breadcrumbs_show') == "Yes") : ?>
         <?php get_template_part('templates/ajax_container'); ?>
        	<?php endif; ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title"><?php //the_title(); ?></h1>
            <?php } ?>
         
            <div class="single-content"><?php the_content();?></div><!-- single content-->

                   
        
        <!-- #comments start-->
        <?php 
        if ( comments_open() || get_comments_number() ) :
            comments_template('', true);
        endif;
        ?>	
        <!-- end comments -->   
        
        <?php endwhile; // end of the loop. ?>
    </div>
  
  </div>  
<?php  include(locate_template('sidebar.php')); ?>
</div>   
<?php get_footer(); ?>